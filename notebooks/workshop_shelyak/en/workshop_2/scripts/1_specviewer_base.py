import sys

import astropy.units as u
from astropy.io import fits
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from specutils.manipulation import extract_region
from astropy.modeling import models, fitting
from specutils.fitting import fit_generic_continuum

# Imports maths, data et visualisation
import matplotlib.pyplot as plt
from matplotlib import colors
import plotly.graph_objects as go
import plotly.express as px
import numpy as np


# Modification of the imports to be able to display the graph
from plotly.offline import download_plotlyjs, init_notebook_mode,  plot
from plotly.graph_objs import *
init_notebook_mode()

"""
Let's go back to the code of our first workshop to simply visualize
an already reduced spectra file, thanks to specutils and matoplotib :
    

    ===== Script goal ===
    Creation of a spec1D
    Display with matplotlib
    ==========================
    

    ===== Elements =====
    Organiaation of the layout
    Help/Doc : Windows(CMD) + I
    Visualization of variables
    Errors and warnings indicators
    Breakpoints / Débugging
    Output of graphs
    ============================
    

"""

#----- Spectrum preparation -----#
# open file
filepath_spectre='data/_28tau_20131215_916.fits'

f = fits.open(filepath_spectre)
f.info()


header = f[0].header
data = f[0].data
print(repr(header))


# Spec Preparation
# Flux units
flux = data * u.Jy

# with fitswcs
wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                               'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                               'CRPIX1': header['CRPIX1']})

# Creating a Spectrum1D object with specutils
spec = Spectrum1D(flux=flux, wcs=wcs_data)


#---- Display ----#

# Figure init
fig01, ax01 = plt.subplots(figsize=(16, 9))

# Definition of the values to be displayed, X and Y
# We also specify the units for the wavelength
ax01.plot(spec.spectral_axis * u.AA, spec.flux)

# X title axis
ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])

# Y title axis
ax01.set_ylabel('Relative Flux')

# Configuration of the grid
ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
    
# Legend
legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
ax01.legend([legend_value], loc=('best'))


# Creating a title and adding it to the chart
spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)

# record in png
#plt.savefig('spectrum.png')

# Display
plt.show()
