#!/usr/bin/env python
# coding: utf-8

# # Python for astronomy and spectroscopy: Rapid spectrum analysis

# This notebook contains the code that was used to create the eponymous article, available at this address: https://stellartrip.net/python-for-astronomy-and-spectroscopy-rapid-spectrum-analysis/?lang=en.
# 
# The dataset used for this notebook can be downloaded at this address: http://e.pc.cd/2MhotalK
# 
# Author : MLA

# ## Coordinates

# ### 1- Retrieving the coordinates of the star gam Cas 

# #### 1.1 - Using Astropy - Coordinates
# import of the Astropy library's coordinates management module

# In[1]:


#!pip install astropy
from astropy.coordinates import SkyCoord


# A SkyCoords object represents an ICRS position (Right ascension [RA], Declination [Dec]) in the sky.
# 
# It is thus possible to retrieve the position of our target, by calling the from_name method which takes the name of the object as a parameter, and then displays its value.

# In[2]:


gam_cas_coords = SkyCoord.from_name("HD5394")
print(f"Objet SkyCoords : {gam_cas_coords}")


# From this variable, which is a SkyCoord object, it is possible to access the separate values for right ascension and declination. Depending on the methods used, different data formats and fineness are accessible.

# In[3]:


print(f" - Position RA/DEC (hms & dms / ep=J2000): {gam_cas_coords.to_string('hmsdms')}")
print(f" - Position RA et DEC (ep=J2000) : {gam_cas_coords.ra.hms, gam_cas_coords.dec.dms}")
print(f" - RA (min) : {gam_cas_coords.ra.hms.m}")


# In the other direction, we can also create a SkyCoord object from the known coordinates and their format. 

# In[4]:


from astropy import units as u
c = SkyCoord(14.184, 60.7249, frame='icrs', unit='deg')
c = SkyCoord('00h56m44.16s', '+60d43m29.64s', frame='icrs')
c = SkyCoord('00 56 44.16 +60 43 29.64', unit=(u.hourangle, u.deg))
c  


# #### 1.2 - Use of Astroquery and the SIMBAD module to retrieve the coordinates and spectral type of gam Cas
# 
# In addition, it is also possible to query the different catalogues with the Astroquery library, as in the example below with a query to SIMBAD, whether it is a sky object or an entire region (https://astroquery.readthedocs.io/en/latest/simbad/simbad.html). The list of catalogue modules is available here : https://astroquery.readthedocs.io/en/latest/
# 
# In addition, adding the "sptype" field to the VOTable query also makes it possible to retrieve the spectral type of the star. The list of possible criteria is available here: http://simbad.u-strasbg.fr/simbad/sim-fsam.

# In[5]:


#!pip install astroquery

#import module
from astroquery.simbad import Simbad

#get a simbad instance
simbad = Simbad()

#add spectral type parameters for VOTable request
simbad.add_votable_fields('sptype')

#request
result_table = simbad.query_object("gam cas")
result_table.pprint(show_unit=True)

#Coordinates
print("\nCoordinates")
print(result_table['RA'][0])
print(result_table['DEC'][0])

#Spectral Type
print("\nSpectral Type")
print(result_table['SP_TYPE'])


# #### 1.3 - Displaying the target within an Aladdin widget
# 
# Previously retrieved tables can then be displayed on the Aladdin widget (in the centre of the cross here for gam Cas). It is also possible to query a region rather than a separate object with "Simbad.query_region", more information on its installation and sources used here : https://github.com/cds-astro/ipyaladin

# In[2]:


import ipyaladin.aladin_widget as ipyal
aladin = ipyal.Aladin(target='gam cas', fov = 1)
aladin


# In[ ]:


#Show tables on Aladin widget
aladin.add_table(result_table)

#record render in jpeg
aladin.get_JPEG_thumbnail()


# A display of different surveys will also make it possible to compare our target group.

# In[3]:


from ipywidgets import Layout, Box, widgets

a = ipyal.Aladin(layout=Layout(width='33.33%'), target='gam cas', fov=0.3)
b = ipyal.Aladin(layout=Layout(width='33.33%'), survey='P/DSS2/red')
c = ipyal.Aladin(layout=Layout(width='33.33%'), survey='P/2MASS/color')

# synchronize target between 3 widgets
widgets.jslink((a, 'target'), (b, 'target'))
widgets.jslink((b, 'target'), (c, 'target'))

# synchronize FoV (zoom level) between 3 widgets
widgets.jslink((a, 'fov'), (b, 'fov'))
widgets.jslink((b, 'fov'), (c, 'fov'))

items = [a, b, c]

box_layout = Layout(display='flex',
                    flex_flow='row',
                    align_items='stretch',
                    border='solid',
                    width='100%')
box = Box(children=items, layout=box_layout)
box


# There are other visualization modules just as interesting, but in order not to extend this article, here are the official links : 
# - PyESASky : https://www.cosmos.esa.int/web/esdc/pyesasky
# - PyWWT : https://pywwt.readthedocs.io/en/stable/installation.html

# ## Source files and visualization of observation data

# ### 2 - Manipulating FITS files with Astropy
# 
# Official documentation and useful sources : 
# - https://learn.astropy.org/FITS-images.html
# - https://learn.astropy.org/rst-tutorials/FITS-tables.html?highlight=filtertutorials
# 
# #### 2.1 - FITS header and metadata

# In[9]:


import warnings #disable warnings for clear output
warnings.filterwarnings("ignore", category=UserWarning)

#Import Mathematics
import numpy as np

# Imports for Visualisation
# matplotlib and use a nicer set of plot parameters
import matplotlib
import matplotlib.pyplot as plt
get_ipython().run_line_magic('config', 'InlineBackend.rc = {}')
get_ipython().run_line_magic('matplotlib', 'notebook')

from astropy.visualization import astropy_mpl_style
plt.style.use(astropy_mpl_style)# set astropy Graph style for plot
from astropy.io import fits #fits manipulation


# In[10]:


#open image and show file info
image_path = 'dataset/gamcas_siril_astrometry.fit'
hdu_list = fits.open(image_path)
hdu_list.info()


# Once the FITS file is loaded into memory, it is possible to read the raw data, as well as the header to view the metadata.

# In[11]:


hdu = hdu_list[0]
image_data = hdu_list[0].data
image_header = hdu_list[0].header

print(f'Image Data Type : {type(image_data)} - Shape : {image_data.shape}')
print('------------------------------ Header ------------------------------')
print(repr(image_header))
print('---------------------------- End Header ----------------------------')


# #### 2.2 - Viewing the image with MatplotLib

# With different types of visualization according to the parameters when calling the imshow() method.

# In[12]:


#Init figure
f1 = plt.figure()
f1.add_subplot()

#Show raw image - Parameters dataset-image & colorMap
plt.imshow(image_data, cmap='gray')
plt.show()


# <b>Characteristics of the image</b>

# To display information about the shooting characteristics, the numpy library can be associated with the numpy library. This allows the statistical values (min, max, avg, standard deviation) to be displayed in addition to the histogram of the image.
# 
# Please note that this is a 32 bit floating point image. (float). The values will of course be different when working with a 16 bit image (int).

# In[13]:


print('----- Statistics values -----')
print('Min :', np.min(image_data))
print('Max :', np.max(image_data))
print('Mean :', np.mean(image_data))
print('Stdev :', np.std(image_data))
print('Data Type :', image_data.dtype) #i.e. <f4 = little-endian single-precision floating point 32 bit 
#(More detail about stype at https://numpy.org/doc/stable/reference/arrays.dtypes.html)
print('Image length : ', len(image_data)) # size list
print('Shape :', image_data.shape) # dimensions of the array


# In[14]:


np.seterr(divide='ignore') #suppress the warnings raised by taking log10 of data with zeros

#New figure
f2 = plt.figure()

#Prepare Histogram
#Because image is a 2D Tab, need to convert in 1-D for plotting
#Use flatten () method on an array return 1-D numpy tab.
plt.hist(np.log10(image_data.flatten()), range=(-3, 2), bins=1000);

#Show Histogram
plt.show()


# We will thus be able to modify the display of our image according to the areas of the histogram that interest us most (i.e. -0.2 / 0.2), because their visualization is facilitated.

# In[15]:


f3 = plt.figure()
img = plt.imshow(image_data)
img.set_clim(-0.1,0.3)
plt.colorbar()


# But also use different types of colouring (cmap) and a logarithmic scale with numpy ( np.log(dataset-image) ).

# In[16]:


#hide warnings
import warnings
warnings.filterwarnings("ignore", category=UserWarning)

from matplotlib.colors import LogNorm

f4 = plt.figure()
f4.suptitle('gam Cas with different visualisation type')

f4.add_subplot(2,2, 1)
plt.imshow(np.log10(image_data), cmap='plasma', vmin=-3, vmax=0, origin='lower')
plt.colorbar()

f4.add_subplot(2,2, 2)
plt.imshow(np.log10(image_data), cmap='viridis', vmin=-3, vmax=0, origin='lower')
plt.colorbar()

f4.add_subplot(2,2,3)
plt.imshow(np.log10(image_data), cmap='hot', vmin=-3, vmax=0, origin='lower')
plt.colorbar()

f4.add_subplot(2,2, 4)
plt.imshow(np.log10(image_data), cmap='nipy_spectral',  vmin=-3, vmax=0, origin='lower')
plt.colorbar()

plt.show(block=True)

#More params at https://matplotlib.org/3.1.1/tutorials/colors/colormapnorms.html


# ### 3 - Celestial positioning of the image 

# When processing the image with the Siril software (https://www.siril.org/), an astrometric resolution was made and the result was added to the header of the fit image. It is possible to project these coordinates on our display (ax = plt.subplot(projection=wcs_for_plot)) with Matplotlib after instantiation of a WCS object of the module of the same name from Astropy. 
# 
# It is also possible to draw the contours of the objects, according to the different levels detected in the matrix.

# #### 3.1 - Displaying a coordinate layer via the Header

# In[17]:


get_ipython().run_line_magic('matplotlib', 'inline')
from astropy.wcs import WCS
import numpy as np
from matplotlib.colors import LogNorm
import warnings
warnings.filterwarnings("ignore", category=UserWarning)#delete after also
#warnings.filterwarnings("ignore", category=Warning)#delete after also

#Create WCS object
wcs_for_plot = WCS(image_header)

#prepare plot
fig5 = plt.figure(figsize=(20, 20))
ax = plt.subplot(projection=wcs_for_plot) #add wcs informations on plot
plt.imshow(image_data, origin='lower', cmap='gray', aspect='equal',norm=LogNorm())
plot_title = "Gamma Cassiopae "
plt.title(plot_title ,fontsize=16, loc='left')

#add and configure axis informations
ax = plt.gca()
ax.grid(True, color='white', ls='solid')
ra = ax.coords[0]
dec = ax.coords[1]

ra.set_axislabel('RA (J2000)',  minpad=1)
dec.set_axislabel('DEC (J2000)',  minpad=1)
ra.set_major_formatter('hh:mm:ss.s')
dec.set_major_formatter('dd:mm:ss.s')

#add other grid as overlay
overlay = ax.get_coords_overlay('icrs') # or galactic, alt-az, ...
overlay.grid(True, color='lime', ls='solid')
overlay[0].set_axislabel('RA (deg)')
overlay[1].set_axislabel('DEC (deg)')

#uncomment the line after for show contour on objects
#ax.contour(image_data, transform=ax.get_transform(wcs_for_plot), levels=np.logspace(-3, 0, 3), colors='orange', alpha=1)

#show
plt.show()


# #### 3.2 - Retrieval of coordinates by the astrometry.net service

# To be able to retrieve the coordinates corresponding to the image, a request to the astrometry service (astrometry.net) is made through the AstroQuery library.
# 

# <b>Converting the FITS image to JPEG</b>

# As the initial image is 80 MB, it is possible to convert this FITS image to jpeg/png, before using the astrometry service, in order to have a lighter image of about 100kb and speed up the process.

# In[34]:


#image name (FITS) to convert : image_path
#image name convert in jpeg/png : gamcas_jpeg.jpeg
from matplotlib.colors import LogNorm
get_ipython().run_line_magic('matplotlib', 'inline')
fig6 = plt.figure(figsize=(16, 9))
plt.imshow(image_data, cmap='gray', norm=LogNorm())
plt.grid()
plt.axis('off')
plt.savefig("dataset/gamcas_jpeg.jpeg", bbox_inches='tight')


# <b>Astrometry.net API Query</b>

# It is necessary to specify the API key linked to your account. This is available on your account page, at this address : http://nova.astrometry.net/api_help. The present warning is normal and simply asks to specify our configuration.

# In[20]:


import warnings #disable warnings for clear output
warnings.filterwarnings("ignore", category=Warning)

#Import astroquery/astrometry library and World Coordinate System module from astropy
from astroquery.astrometry_net import AstrometryNet
from astropy.wcs import WCS


ast = AstrometryNet()
ast.api_key = '##############' # PUT YOUR API KEY HERE

try_again = True
submission_id = None

while try_again:
    try:
        if not submission_id:
            wcs_header = ast.solve_from_image('dataset/gamcas_jpeg.jpeg',
                                              submission_id=submission_id)
        else:
            wcs_header = ast.monitor_submission(submission_id,
                                                solve_timeout=120)
    except TimeoutError as e:
        submission_id = e.args[1]
    else:
        # got a result, so terminate
        try_again = False


#when solve is ok, record data in wcs_gamcas object
if wcs_header:
    print('Solve OK - Print wcs result ...')
    wcs_gamcas = WCS(wcs_header)
    print(wcs_gamcas)
    
else:
    print('nok')
      


# <b>Display of celestial coordinates after return from the Astrometry.net API</b>

# The coordinates can then be displayed over the image in the desired coordinate system. More information about the Astropy WCS management system at these addresses :
# - https://het.as.utexas.edu/HET/Software/Astropy-1.0/wcs/index.html
# - https://docs.astropy.org/en/stable/visualization/wcsaxes/initializing_axes.html

# In[33]:


import matplotlib.image as mpimg
    
#Prepare plot
img = mpimg.imread('dataset/gamcas_jpeg.jpeg') #Image name_file
fig7 = plt.figure(figsize=(20, 20))

ax = plt.subplot(projection=wcs_gamcas) #Add wcs projection with wcs_gamcas file
plt.imshow(img, origin='lower', cmap='gray', aspect='equal',norm=LogNorm())

plt.title('gam Cas - Astrometry.net API',fontsize=16, loc='center')

ax.coords['ra'].set_axislabel('RA')
ax.coords['dec'].set_axislabel('DEC')

overlay = ax.get_coords_overlay('icrs')
overlay.grid(color='red', ls='dotted')

plt.show()


# <b>Retrieving and displaying coordinates without going through the API in line of code</b>

# In the event that astrometric resolution is not performed by the Python API via the previous piece of code. Typical use via the FITS file upload on astrometry.net will retrieve the single WCS file, or the FITS image including the WCS header. The code below shows the display of coordinates on the image using this method. The file retrieved from astrometry.net is called 'wcs.fit'

# In[22]:


#Get wcs file path
wcs_file = fits.open('dataset/wcs.fits')
header_wcs_file = wcs_file[0].header

#Create WCS object
wcs_for_plot2 = WCS(header_wcs_file)

#Image plot must be the same than upload image on astrometry.net
import matplotlib.image as mpimg
img = mpimg.imread('dataset/gamcas_jpeg.jpeg')

fig8 = plt.figure(figsize=(20, 20))
ax = plt.subplot(projection=wcs_for_plot2)
plt.imshow(img, origin='lower', cmap='gray', aspect='equal',norm=LogNorm())

plot_title = "Gamma Cassiopae"
plt.title(plot_title ,fontsize=16, loc='left')

ax.coords['ra'].set_axislabel('RA')
ax.coords['dec'].set_axislabel('DEC')

overlay = ax.get_coords_overlay('icrs')
overlay.grid(color='red', ls='dotted')

plt.show()


# ### 4 - Display and manipulation of spectra processed with SpecUtils
# 
# #### 4.1 - Visualisation of the 2D Spectrum

# From the elements used previously, it is therefore possible to display the acquired raw spectrum and check that it is not saturated.

# In[23]:


#open image and show file info
spec1D_path = 'dataset/Navi_Light_007.fits'
hdu_list_spec1D = fits.open(spec1D_path)
hdu_list_spec1D.info()

spec1D_data = hdu_list_spec1D[0].data
spec1D_header = hdu_list_spec1D[0].header

print(f'Image Data Type : {type(spec1D_data)} - Shape : {spec1D_data.shape}')
print('------------------------------ Header ------------------------------')
print(repr(spec1D_header))
print('---------------------------- End Header ----------------------------')

fig9 = plt.figure()
plt.imshow(spec1D_data, cmap='gray',norm=LogNorm(), vmin=1800,vmax = 4500)
plt.grid(False)

print('Statistics values')
print('Min:', np.min(spec1D_data))
print('Max:', np.max(spec1D_data))
print('Mean:', np.mean(spec1D_data))
print('Stdev:', np.std(spec1D_data))


# Note that the reduction part of the 2D to 1D spectrum is not mentioned here. It is considered that this processing is carried out with current software  (i.e. ISIS, Demetra, VSpec, SpcAudace, etc.).
# - ISIS : http://www.astrosurf.com/buil/isis-software.html
# - Demetra : https://www.shelyak.com/logiciel-demetra/
# - VSpec : http://astrosurf.com/vdesnoux/
# - SpcAudace : http://spcaudace.free.fr/

# #### 4.2 - 1D Spectrum Visualisation with SpecUtils and Matplotlib

# In[24]:


#Show a unique spectrum 

#imports
from astropy import units as u #units
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)

spec_path = "dataset/GamCas_20201104T204413.fit"

#open & load spectrum file
file = fits.open(spec_path)  
specdata = file[0].data
header = file[0].header

#make WCS object
wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                               'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                               'CRPIX1': header['CRPIX1']})

#set flux units
flux= specdata * u.Jy

#create a Spectrum1D object with specutils
spec = Spectrum1D(flux=flux, wcs=wcs_data)

#plot spectrum
fig, ax = plt.subplots(figsize=(16, 9))
ax.plot(spec.spectral_axis * u.AA, spec.flux)

#X axis label
ax.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])

#Y axis label
ax.set_ylabel('Relative Flux')

#Grid configuration
ax.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
    
#legend configuraiton
legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
ax.legend([legend_value], loc=('best'))

#prepare and set plot title with header infos 
spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - '+ header['EXPTIME2']+ ' - ' + str(header['DETNAM'])
ax.set_title(spectrumTitle, loc='center', fontsize=12, fontweight=0.5)

#Show Plot
plt.show()


# #### 4.3 - Spectrum normalization by its continuum

# <b>Continuum display</b>

# In[25]:


#Visualize continuum fitting

#imports
from astropy.modeling import models, fitting
from specutils.fitting import fit_generic_continuum

#prepare data
x = spec.spectral_axis
y = spec.flux

#fitting continuum (with exclude region between 3700A and 4000A)
g1_fit = fit_generic_continuum(spec, exclude_regions=[SpectralRegion(3700 * u.AA, 4000 * u.AA)])
y_continuum_fitted = g1_fit(x)

#make plot
fig10, ax10 = plt.subplots(figsize=(8,5))
#spectrum
ax10.plot(x, y)
#continuum
ax10.plot(x, y_continuum_fitted, color="orange")
ax10.grid(True)
plt.show()


# <b>Division by the continuum</b>

# In[26]:


#Normalization

#prepare data
x_2 = spec.spectral_axis
y_2 = spec.flux

#fitting continuum (with exclude region 3700/4000 & H lines emission)
g_fit = fit_generic_continuum(spec, exclude_regions=[SpectralRegion(3700 * u.AA, 4000 * u.AA), SpectralRegion(4825 * u.AA, 4885 * u.AA), SpectralRegion(6520 * u.AA, 6540 * u.AA)])

#divide spectrum by his continuum
y_cont_fitted = g_fit(x_2)
spec_normalized = spec / y_cont_fitted

#show on plot
fig11, ax11 = plt.subplots(figsize=(8,5))
ax11.plot(spec_normalized.spectral_axis, spec_normalized.flux)
ax11.grid(True)
plt.show()


# #### 4.4  - Emission line detection

# Once the spectrum has been normalised, it is possible to use the find_lines_derivative method to detect emission or absorption lines in the spectrum. There are two methods for this step, by calculating the derivative and then thresholding the flux (as below), or by thresholding the flux according to a factor applied to the spectral uncertainty. More information on these methods here : https://specutils.readthedocs.io/en/stable/fitting.html

# In[27]:


#imports
from specutils.fitting import find_lines_derivative
from specutils.fitting import fit_lines

#Line fitting with Derivative technique
lines = find_lines_derivative(spec_normalized, flux_threshold=1.2)
print('emission: ', lines[lines['line_type'] == 'emission']) 


# #### 4.5 - Selection of a spectral region and display of line analysis values

# From the detection of lines in previous emissions, the selection of an associated spectral region allows to focus on one of them, Halpha for example, and to use existing analysis tools to retrieve the values (i.e. centroid, fhwm, snr, estimation of parameters of a Gaussian model, etc.). More information on these analysis tools here  : https://specutils.readthedocs.io/en/stable/fitting.html#parameter-estimation

# In[28]:


#import analysis tools
from specutils.manipulation import extract_region
from specutils.fitting import estimate_line_parameters
from specutils.analysis import centroid, fwhm

#create spectral region (after line detection of 6562.77A) +/- 50A
sr =  SpectralRegion((6563-50)*u.AA, (6563+50)*u.AA)

#print centroid - need a spectrum and a spectral region in parameters
center = centroid(spec_normalized, sr)  
print("center : ", center)

#print fwhm - need a spectrum and a spectral region in parameters
fwhm_spec = fwhm(spec_normalized, regions=sr)
print("fwhm : ", fwhm_spec)

#create a new spectrum of the selected region for plot
sub_spectrum = extract_region(spec_normalized, sr)
Ha_line = Spectrum1D(flux=sub_spectrum.flux,spectral_axis=sub_spectrum.spectral_axis)

#plot
fig12, ax12 = plt.subplots(figsize=(8,5))
ax12.plot(Ha_line.spectral_axis, Ha_line.flux)
ax12.grid(True)
plt.show()


# #### 4.6 - Multi-spectrum visualization

# When several spectra are acquired over time on the same target, it is interesting to visualise them on the same graph to be able to compare them. Portions of previous codes can thus be reused. For the example below, I've double the two gam Cas spectra files made on the same night to make it more visible.

# In[29]:


#imports if needed
from astropy.io import fits
import astropy.units as u
import astropy.wcs as fitswcs
from specutils import Spectrum1D
import matplotlib.pyplot as plt

#list of path files
spec_list = ["dataset/GamCas_20201104T204413.fit",
            "dataset/GamCas_20201104T204413-2.fit",
            "dataset/GamCas_20201104T204937.fit",
            "dataset/GamCas_20201104T204937-2.fit"] 

#list of spectrum1D
spec1D_list = []

#modify offset for change visualization preferences
offset = 0.3

#parse and create plot for each spectrum in progress
for sip in spec_list :
    sip_file = fits.open(sip)
    sip_data = sip_file[0].data
    sip_header = sip_file[0].header

    #create WCS object for each spectrum in progress
    sip_wcs = fitswcs.WCS(header={'CDELT1': sip_header['CDELT1'], 'CRVAL1': sip_header['CRVAL1'],
                                   'CUNIT1': sip_header['CUNIT1'], 'CTYPE1': sip_header['CTYPE1'],
                                   'CRPIX1': sip_header['CRPIX1']})
    
    #apply offset on flux and create spec1D
    sip_flux = (sip_data + (offset * (spec_list.index(sip)))) * u.Jy
    sip_spec1D = Spectrum1D(flux=sip_flux, wcs=sip_wcs)
    
    #add spec1D to spec1DList
    spec1D_list.append((sip_spec1D, sip_header))
        
        
#create a fig
fig = plt.subplots(figsize=(16, 9))

#create a line plot for each spec1D in the list
for spec1Dip in spec1D_list:
    plt.plot(spec1Dip[0].spectral_axis, spec1Dip[0].flux, label=spec1Dip[1]["DATE-OBS"])
    
#Plot configuration 
plt.legend(loc='upper right')
plt.xlabel("Wavelenght ($\lambda$) in $\AA$ ")
plt.ylabel("Relative Flux")
plt.title("gamCas - Spectrum Comparaison - Alpy600", fontsize=16, fontweight='bold')
plt.show()


# #### 4.4 - Additional libraries

# The code for the implementation of the Bokeh library (see specBok) can be found at this address :
# 
# https://gitlab.com/chronosastro/aspyt
#     
# An example will be added in this notebook later.

# ------

# Thank you for taking the time to read so far ! :)
# 
# Contact : https://stellartrip.net/contact

# ##### Sources and further information (update in progress)

# <sup>1</sup> http://www.astrosurf.com/buil/isis-software.html
# 
# <sup>2</sup> https://www.shelyak.com/logiciel-demetra/
# 
# <sup>3</sup> http://astrosurf.com/vdesnoux/
# 
# <sup>4</sup> https://jupyter.org/
# 
# <sup>5</sup> https://fr.wikipedia.org/wiki/Markdown
# 
# <sup>6</sup> https://en.wikipedia.org/wiki/Astropy
# 
# <sup>7</sup> https://www.astropy.org/
# 
# <sup>8</sup> http://simbad.u-strasbg.fr/simbad/
# 
# <sup>9</sup> https://learn.astropy.org/FITS-images.html
# 
# <sup>10</sup> https://matplotlib.org/
# 
# <sup>11</sup> https://learn.astropy.org/rst-tutorials/FITS-tables.html?highlight=filtertutorials
# 
# <sup>12</sup> https://www.siril.org/
# 
# <sup>13</sup> https://docs.astropy.org/en/stable/visualization/wcsaxes/ticks_labels_grid.html
# 
# <sup>14</sup> http://spcaudace.free.fr/
# 
# <sup>15</sup> https://specutils.readthedocs.io/en/stable/index.html

# In[ ]:




