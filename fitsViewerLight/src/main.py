#########
# IMPORT #
#########

import os
import platform
import logging
from datetime import datetime
from time import gmtime, strftime

from PySide2 import QtGui, QtWidgets, QtCore
from PySide2.QtUiTools import QUiLoader
from astropy.io import fits
import sys, re

from menu.openFile import fileChooser
from menu.openSpectrum import spectrumChooser
from menu.closeFile import closePicture
from utils.fitsManager import FitsManager
from utils.spectrumManager import SpectrumManager


class Viewer(QtWidgets.QMainWindow):
    """Main display class
    
    Attributes
    ----------
    ui : PySide2.QtWidgets.QMainWindow
        main display
    
    Methods
    -------
    load()
        loads the interface. Must be followed by .show() method to display the window
    dragEnterEvent(event)
        Allows to retrieve the information of the file that is put on the window
    dropEvent(event)
        Allows you to manage the actions that will be performed when the file is dropped on the window 
    """


    def load(self):

        """
        loads the interface. Must be followed by .show() method to display the window
        """
        self.setWindowTitle("Stellartrip FitsViewerLight")
        loader = QUiLoader()
        self.ui = loader.load('view/interface.ui')
        self.setAcceptDrops(True)
        self.ui.setAcceptDrops(True)
        print("1 - Load ui ok and accept drops")

        #create link for menu and action -> close picture / initUI etc..
        fc = fileChooser(parent=self)
        sc = spectrumChooser(parent=self)
        cp = closePicture(parent=self)
        self.ui.actionOpen_file.triggered.connect(lambda checked: fc.initUI())
        self.ui.actionOpen_Spectrum.triggered.connect(lambda checked: sc.initUI())
        self.ui.actionClose_Picture.triggered.connect(lambda checked: cp.close())
        self.ui.actionQuit.triggered.connect(quit)

        #Retrieve the button to save modifications to fits file's header, add the method to handle saving
        #TODO rename list -> in TabWidgetlist
        list = self.ui.findChildren(QtWidgets.QTableWidget, 'tableWidget')[0]

        updatefits_btn = self.ui.findChildren(QtWidgets.QPushButton, 'updatefits_btn')[0]
        updatefits_btn.clicked.connect(lambda: FitsManager.editFitsFileHeader(list,self))

        change_theme_btn = self.ui.findChildren(QtWidgets.QCheckBox, 'cybpk_ckbox')[0]
        change_theme_btn.clicked.connect(lambda: SpectrumManager.set_cbk_style(self))

        show_ax2_btn = self.ui.findChildren(QtWidgets.QCheckBox, 'second_axis_ckbox')[0]
        show_ax2_btn.setChecked(True)
        show_ax2_btn.clicked.connect(lambda: SpectrumManager.show_ax2(self))

        self.setCentralWidget(self.ui)
      
    def dragEnterEvent(self, event):
        """Allows to retrieve the information of the file that is put on the window

        Parameters
        ----------
        event : PySide2.QtGui.QDragEnterEvent
            Event that is triggered when the file is on the window. Contains information such as the path to the file.
        """

        mime = event.mimeData()
        urls = mime.urls()[0]
        if len(mime.urls()) == 1 and re.match("PySide2.QtCore.QUrl\('[\w\W]*.(fits|fit)\'\)", str(urls)) != None :
            event.acceptProposedAction()


    def dropEvent(self, event):
        """Allows you to manage the actions that will be performed when the file is dropped on the window 
    
        Parameters
        ----------
        event : PySide2.QtGui.QDragEnterEvent
            Event that is triggered when the file is on the window. Contains information such as the path to the file.
        """

        for url in event.mimeData().urls():
            file_name = url.toLocalFile()
            header = fits.open(file_name)[0].header
            # TODO add more verification for Spectrum if possible
            if ('CDELT1' in header and 'CRVAL1' in header and 'CRPIX1' in header and 'NAXIS1' in header):
                SpectrumManager.loadFitsFile(file_name, self)
            else:
                #If image -> launch FitsManager
                FitsManager.loadFitsFile(file_name, self)
        return super().dropEvent(event)


app = QtWidgets.QApplication(sys.argv)
app.setWindowIcon(QtGui.QIcon('stellartrip.png'))
window = Viewer()
window.load()

window.resize(1600, 800)
window.show()
app_return_code = app.exec_()
print(app_return_code)
sys.exit(app_return_code)

